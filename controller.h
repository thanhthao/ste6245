#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "plane.h"
#include "ball.h"
#include "collobj.h"
// GMlib
#include <core/gmarray>
// Qt
#include <QDebug>
#include <stdexcept>
#include <iostream>

class Controller:public GMlib::SceneObject{
    GM_SCENEOBJECT(Controller)
private:

    GMlib::Array <Ball*> ball_array;
    GMlib::Array <Plane*> plane_array;
    GMlib::Array <CollObj> collision ;

public:

    Controller();
    void insertScene();

protected:

    void findBBColl(Ball *b1,Ball *b2, GMlib::Array<CollObj> &co,double y);
    void findBWColl(Ball *b,Plane *w, GMlib::Array<CollObj> &co,double y);
    void localSimulate(double dt);   //virtual function, inherit from SceneObject
    void collBB(Ball *b1,Ball *b2,double dt);
    void collBW(Ball *b,Plane *w,double time);

};


#endif // CONTROLLER_H


