#include "controller.h"
#include <core/gmarray>
#include <parametrics/gmpbeziersurf>

#include <QDebug>

Controller::Controller(){

    this->toggleVisible();
    insertScene();
}

void Controller::findBBColl(Ball *b1,Ball *b2, GMlib::Array<CollObj> &co,double y){

    GMlib::Vector<float,3> ds1 = b1->get_ds();
    GMlib::Vector<float,3> ds2 = b2->get_ds();
    GMlib::Point<float,3>  p1  = b1->getPos();
    GMlib::Point<float,3>  p2  = b2->getPos();

    float x1 = b1->getX();
    float x2 = b2->getX();
    float r1 = b1->getRadius();
    float r2 = b2->getRadius();
    double r = r1 + r2;

    GMlib::Vector<float,3> h = (1 + x1)*ds1 - (1 + x2)*ds2;
    GMlib::Vector<float,3> s = p1 - p2 - x1*ds1 + x2*ds2;

    double a = h * h;
    double b = s * h;
    double c = (s*s) - (r*r);
    double d = b*b - a*c;

    // to avoid 2 balls inside each other:
    // check that if sum of radius > length of vector of 2 center points
    // just translate 2 balls in d1,d2 direction(vector d1 = p1-p2,d2 = p2-p1)a little that opposite each other
    // because we do not know which direction these 2 balls move
    double temp = r - (p1-p2).getLength();
    if(temp > 0){
        qDebug()<<"ball ball inside : " << temp;
        b1->translate(temp*(p1-p2));
        b2->translate(temp*(p2-p1));

        s *= 1 + 2 * temp;
        b = s * h;
        c = (s*s) - (r*r);
    }


    if(a < 1e-6){ //a<10^-6
        // qDebug()<< "2 balls run parallel,ds1=ds2" ;
    }
    else if (d <= 0){
        // qDebug()<< "not touch other balls" ;
    }
    else if (d > 0){
        double x = (-b-sqrt(d))/a;
        if(x > y && x <= 1){
            qDebug()<< " BB colision " << x  ;
            co.insertAlways(CollObj(b1,b2,x),true);
        }
    }
    // else qDebug()<< "no collision between the balls" ;

}

void Controller::findBWColl(Ball *b,Plane *w, GMlib::Array<CollObj> &co, double y){

    GMlib::UnitVector<float,3> n = w->getNormal();
    GMlib::Vector<float,3>     d = w->getCornerPoint() - b->getPos();

    double dn  = d * n;
    double dsn = b->get_ds() * n;
    double r   = b->getRadius();

    // check that if the ball outside the wall,translate it into the ground,inside the wall

    if(dn + r > 0){
        b->translate(2 *(dn+r)*n);
        qDebug()<<"translate:" << dn + r;
        dn -= 2 *(dn + r);
    }

    if(std::abs(dsn) < 1.0e-6){     // <10^-6 ,too small
        // make the ball going paralell to the wall
         b->set_ds(b->get_ds()-(b->get_ds()*n)*n);
         qDebug()<<"dsn too small "<< dsn;

    }
    else {
        double  x = (r + dn)/dsn;
        qDebug()<<"dsn = "<< dsn;
        qDebug()<<"x = "<< x;
        if( x > y && x <= 1){   // y: khoang thoi gian de co duoc collision

            co.insertAlways(CollObj(b,w,x),true);
            qDebug()<<"collision with the wallllllllllllllllllllllllllllllllllllll";

        }
        else{
            //   qDebug()<<"x > 1 or x <= x1 before";
        }
    }

}

void Controller::localSimulate(double dt){
    GMlib::Array<CollObj> colArray;

    for(int i = 0; i < ball_array.getSize(); i++){
        ball_array[i]->updateStep(dt);
    }

    for(int i = 0; i < ball_array.size(); i++)
        for(int j = i + 1; j<ball_array.size(); j++)
            findBBColl(ball_array[i],ball_array[j],colArray,0);

    for(int i = 0; i < ball_array.size(); i++)
        for(int j = 0; j < plane_array.size(); j++)
            findBWColl(ball_array[i],plane_array[j],colArray,0);



    qDebug()<<"colArray size:"<<colArray.getSize();

    while(colArray.getSize()>0)
    {
        colArray.sort(); //using operator <

        colArray.makeUnique();  //using operator ==
        // find all elements in colArray,if some elements are the same with the first one
        //,delete them to make it unique.

        CollObj co = colArray[0];  // take out the first element
        colArray.removeIndex(0);   //and then delete it in array

        if(co.getBW()){        // check that it is ball-wall collision

            co.getBall(0)->moveBall(co.getX()*dt);                  // move ball to new pos that happens collision with the wal
            collBW(co.getBall(0), co.getPlane(),(1-co.getX())*dt);  //update the ball with the rest time after collision
            co.getBall(0)->setX(co.getX());                         // set the value of x

                // find new collisions

            for(int i = 0; i < ball_array.size(); i++)
                if(ball_array[i] != co.getBall(0))
                    findBBColl(co.getBall(0),ball_array[i],colArray,co.getX());
            for(int i = 0; i < plane_array.size(); i++)
                if(plane_array[i] != co.getPlane())
                    findBWColl(co.getBall(0),plane_array[i],colArray,co.getX());
        }
        else{       // if ball-ball collision
            co.getBall(0)->moveBall(co.getX()*dt); // move ball1 to new pos that happens collision with the ball2
            co.getBall(1)->moveBall(co.getX()*dt); // move ball2 to new pos that happens collision with the ball1
            collBB(co.getBall(0), co.getBall(1),(1-co.getX())*dt); // update 2 balls after collision
            co.getBall(0)->setX(co.getX());       // set the x value of 2 balls
            co.getBall(1)->setX(co.getX());

             // find new collisions
            for(int i = 0; i < ball_array.size(); i++)
                if(ball_array[i] != co.getBall(0) && ball_array[i] != co.getBall(1))
                {
                    findBBColl(co.getBall(0),ball_array[i],colArray,co.getX());
                    findBBColl(co.getBall(1),ball_array[i],colArray,co.getX());
                }
            for(int i = 0; i < plane_array.size(); i++)
            {
                findBWColl(co.getBall(0),plane_array[i],colArray,co.getX());
                findBWColl(co.getBall(1),plane_array[i],colArray,co.getX());
            }
        }
    }

}

void Controller::insertScene(){

    //berzier surf
     GMlib::DMatrix< GMlib::Vector<float,3> > mt;
     mt.setDim(3,3);
     GMlib::Vector<float ,3> m00(0,0,0);
     GMlib::Vector<float ,3> m01(6,0,0);
     GMlib::Vector<float ,3> m02(12,0,0);
     GMlib::Vector<float ,3> m10(0,6,0);
     GMlib::Vector<float ,3> m11(6,6,-12);
     GMlib::Vector<float ,3> m12(12,6,0);
     GMlib::Vector<float ,3> m20(0,12,0);
     GMlib::Vector<float ,3> m21(6,12,0);
     GMlib::Vector<float ,3> m22(12,12,0);

     mt[0][0] = m00;
     mt[0][1] = m01;
     mt[0][2] = m02;
     mt[1][0] = m10;
     mt[1][1] = m11;
     mt[1][2] = m12;
     mt[2][0] = m20;
     mt[2][1] = m21;
     mt[2][2] = m22;


    GMlib::PBezierSurf<float> *ground = new GMlib::PBezierSurf <float>(mt) ;
    ground->toggleDefaultVisualizer();
    ground->replot(20,20,1,1);
    this->insert(ground);
    ground->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);


    // //draw 4 walls

    const GMlib::Point<float,3> p1( 0.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p2( 12.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p3( 0.0, 12.0, 0.0 );

    const GMlib::Vector<float,3> v1(12.0, 0.0, 0.0);
    const GMlib::Vector<float,3> v2(0.0, 12.0, 0.0);
    const GMlib::Vector<float,3> v3(0.0, 0.0, 2.0);

    Plane* left_wall = new Plane(p1,v2,v3);
    left_wall->toggleDefaultVisualizer();
    left_wall->replot(20,20,1,1);
    this->insert(left_wall);


    Plane* right_wall = new Plane(p2,v3,v2);
    right_wall->toggleDefaultVisualizer();
    right_wall->replot(20,20,1,1);
    this->insert(right_wall);

    Plane* front_wall = new Plane(p1,v3,v1);
    front_wall->toggleDefaultVisualizer();
    front_wall->replot(20,20,1,1);
    this->insert(front_wall);

    Plane* behind_wall = new Plane(p3,v1,v3);
    behind_wall->toggleDefaultVisualizer();
    behind_wall->replot(20,20,1,1);
    this->insert(behind_wall);

    //silver ball
    GMlib::Vector<float,3> v4( 1.5, 3.5, 0.0);
    Ball* silver_ball = new Ball(v4,1,1,ground); // mass=1
    silver_ball->toggleDefaultVisualizer();//de xuat hien ball
    silver_ball->replot(20,20,1,1);
    silver_ball->setMaterial(GMlib::GMmaterial::Silver); //nho them truoc insert
    this->insert(silver_ball);
    silver_ball->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
        //change ball posision to the ground
     const GMlib::Vector<float,3> vec(3.0, 2.0, 1.0);
     silver_ball->moveToGround(ground,vec);

    //black ball
    GMlib::Vector<float,3> v5( 7, 5, 0.0);
    Ball* black_ball = new Ball(v5,1,1,ground);
    black_ball->toggleDefaultVisualizer();//de xuat hien ball
    black_ball->replot(20,20,1,1);
    black_ball->setMaterial(GMlib::GMmaterial::BlackPlastic); //nho them truoc insert
    this->insert(black_ball);
    black_ball->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
     //change ball's posision to the ground
    const GMlib::Vector<float,3> vec2(5, 5, 1.0);
    black_ball->moveToGround(ground,vec2);

    //red ball
    GMlib::Vector<float,3> v6( 6.0, 1.5, 0.0);
    Ball* red_ball = new Ball(v6,1,1,ground);
    red_ball->toggleDefaultVisualizer();            //de xuat hien ball
    red_ball->replot(20,20,1,1);
    red_ball->setMaterial(GMlib::GMmaterial::Ruby); //nho them truoc insert
    this->insert(red_ball);
    red_ball->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
      //change ball's posision to the ground
    const GMlib::Vector<float,3> vec3(4, 7, 1.0);
    red_ball->moveToGround(ground,vec3);

    //green ball
    GMlib::Vector<float,3> v7( 4.0, 2.5, 0.0);
    Ball* emerald_ball = new Ball(v7,1,1,ground);
    emerald_ball->toggleDefaultVisualizer();               //de xuat hien ball
    emerald_ball->replot(20,20,1,1);
    emerald_ball->setMaterial(GMlib::GMmaterial::Emerald); //nho them truoc insert
    this->insert(emerald_ball);
    emerald_ball->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
      //change ball posision to the ground
    const GMlib::Vector<float,3> vec4(5, 2, 1.0);
    emerald_ball->moveToGround(ground,vec4);

    //insert balls and planes into array
      ball_array.insertAlways(silver_ball);
      ball_array.insertAlways(black_ball);
      ball_array.insertAlways(red_ball);
      ball_array.insertAlways(emerald_ball);

      plane_array.insertAlways(left_wall);
      plane_array.insertAlways(right_wall);
      plane_array.insertAlways(front_wall);
      plane_array.insertAlways(behind_wall);

}

// update direction of balls after collision with other during the time dt (thoi gian con lai sau tuong tac)
void Controller::collBB(Ball *b1,Ball *b2,double dt){
    GMlib::Vector<float,3> new_vel1 = b1->getVel();//sv1
    GMlib::Vector<float,3> new_vel2 = b2->getVel();//sv2
    GMlib::Point<float,3> p1,p2;
    qDebug()<<"old energy:" << (new_vel1 * new_vel1 + new_vel2 * new_vel2)/2;

    p1 = b1->getPos();
    p2 = b2->getPos();
    GMlib::Vector<float,3> vn1,vn2,vd10,vd20;
    GMlib::UnitVector<float,3> d = p1 - p2;

    vd10 = (new_vel1 * d) * d;//v1
    vn1  =  new_vel1 - vd10;

    vd20 = (new_vel2 * d) * d;//v2
    vn2  =  new_vel2 - vd20;


    float m1 = b1->getMass();
    float m2 = b2->getMass();

    GMlib::Vector<float,3> vd11;
    GMlib::Vector<float,3> vd21;
   // vd11 = ((m1-m2)/(m1+m2)* vd10 + 2*m2/(m1+m2)* vd20)*0.9 ;
  //  vd21 = ((m2-m1)/(m1+m2)* vd20 + 2*m1/(m1+m2)* vd10)*0.9 ;

    vd11 = ((m1-m2)/(m1+m2)* vd10 + 2*m2/(m1+m2)* vd20) ;
    vd21 = ((m2-m1)/(m1+m2)* vd20 + 2*m1/(m1+m2)* vd10) ;

    new_vel1 = vn1 + vd11;
    new_vel2 = vn2 + vd21;

    qDebug()<<"new energy:" <<(new_vel1 * new_vel1 + new_vel2 * new_vel2)/2;

    b1->setVel(new_vel1);
    b1->updateStep(dt);
    b2->setVel(new_vel2);
    b2->updateStep(dt);
    qDebug()<< "velocity ball 1 after collision: " << new_vel1;
    qDebug()<< "velocity ball 2 after collision: " << new_vel2;

}


// update direction of ball after collision with wall
void Controller::collBW(Ball *b,Plane *w,double time){  // how ball + plane after col is ,with time: "time"

    GMlib::Vector<float,3> new_vel = b->getVel();
    qDebug()<<"velocity before collision : " << new_vel;
    new_vel -= 2 * (w->getNormal()* b->getVel()) * w->getNormal();

   //  new_vel -= 1.3 * (w->getNormal()* b->getVel()) * w->getNormal();
   // b->setVelKeepEnergy(new_vel);
    b->setVel(new_vel);

    b->updateStep(time);

    qDebug()<<"velocity after collision with the wall: " << new_vel;

}


