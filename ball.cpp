#include "ball.h"

Ball::~Ball() {}

void Ball::set_ds(const GMlib::Vector<float,3> &s)
{
    ds = s;
}

Ball::Ball(GMlib::Vector<float,3> v, float m, float r, GMlib::PBezierSurf<float> *s):GMlib::PSphere<float>(r){
    this->vel  = v;
    this->surf = s;
    this->mass = m;
    x = 0;
    this->energy = mass*(0.5*(vel*vel)+ 9.81*getPos()(2));

}


void Ball::updateStep(double dt){
    // luc hut cua trai dat, the earth's gravity
    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);// -9.81 means this vector goes down

    //direction to move with the ball's velocity and earth's gravity
    ds = dt*vel + (0.5*dt*dt)*g; //the first + the second derivative

    //getPos() to take the center position of the ball
    // calculate the new position of the ball moving with direction ds after time dt
    p = this->getPos() + ds;

    //get vector d = p-q, with q is the point in the corner of the ground.
    // this point and 2 vector v1,v2 to create the ground
    // q = surf->getCornerPoint() returns _dt (0,0,0)

    //GMlib::Vector<float,3> d = p - surf->getCornerPoint();

    //qDebug()<<"corner point:"<< surf->getCornerPoint();

    //calculate the closed point to go
    //find  u, v
    // use getU() and getV() to get 2 vector v1,v2 that created the plane
    // u=<d,v1>/<v1,v1>   and v=<d,v2>/<v2,v2>

  //  u = (d*surf->getU())/(surf->getU()*surf->getU());
  //  v = (d*surf->getV())/(surf->getV()*surf->getV());
     surf->getClosestPoint(p,u,v);

    //need to get normal n
    //evaluate(float u,float v,int d1,int d2) returns a 2x2 matrix m
    // d1=d2=1 : to take the first derivative to get the normal
    //new position = a+r*n   , a = m[0][0] from evaluate() , r: radius of ball,
    //n: normal vector n = m[0][1]^m[1][0] vector product,or n = surf->getnormal()

    GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(u,v,1,1);
    //std::cout<<"matrix m:"<<m<< std::endl;

    //n = surf->getNormal();// returns UnitVector<T,3> _n in gmplane
    GMlib::UnitVector<float,3> n = m[0][1]^m[1][0];
    GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;


    //update velocity and ds
    // update ds to new position to show in the screen.
    // vector ds = new position - center point of the ball (getPos)
    // new velocity = ds/dt
    ds = newpos - this->getPos();

    // /// moi doi
    double vv= vel*vel;
    vel += g * dt;
    vel -= (vel*n)*n;

    if( vv + 2*(g*ds) < 0 )
        qDebug()<<"<0";


   double nvv = vv - 2*(g*ds);

   if(nvv > 0.0001)
   {
       if( vv>0.001 )
       vel *= std::sqrt(vv/nvv);
     }

    // rotate the ball when moving
    rax = n^this->get_ds();

}

void Ball::setUV(GMlib::PBezierSurf<float> *s)
{
    s->estimateClpPar(this->getPos(),u,v); // estimate closest point

}

void Ball::localSimulate(double dt){  // dt :time from system

    updateStep(dt);
    this->translateParent(ds); //
    this->setX(0); //reset the value x = 0 after 1 moving step
    rotateParent(GMlib::Angle(this->get_ds().getLength()/this->getRadius()), rax );

}

// move ball to the position happening collision with wall or another ball
// calculate the new velocity in the first part time of dt.
// after this,in BBcol or BWcol will update new velocity and new ds after collision
// with the rest time of dt, set value of x = 0 in localSimulate
void Ball::moveBall(double time){

    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);// -9.81 means this vector goes down
    this->translate(time*ds);
    vel += time*g;
    GMlib::UnitVector<float,3> normal = surf->getNormal();
    vel -= (vel*normal)*normal;

}

//first, move the ball to the ground
void Ball::moveToGround(GMlib::PBezierSurf<float> *ground,const GMlib::Vector<float,3> vec){

 this->translate(vec);
 this->setUV(ground);  //find new u,v
 p = this->getPos();
 surf->getClosestPoint(p,u,v);

 GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(u,v,1,1);
 GMlib::UnitVector<float,3> n = m[0][1]^m[1][0];
 GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;

 ds = newpos - this->getPos();
 this->translate(ds);
 GMlib::UnitVector<float,3> normal = surf->getNormal();
 vel -= (vel*normal)*normal;

}
