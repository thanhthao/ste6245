#include "collobj.h"

CollObj::CollObj(){}

CollObj::CollObj(Ball *b1, Ball *b2, double x1){
    b[0] = b1;
    b[1] = b2;
    this->x = x1;
    bw = false;
}

CollObj::CollObj(Ball *b1, Plane *w1, double x1){
    b[0] = b1;
    this->w = w1;
    this->x = x1;
    bw = true;
}

// using for sort() in controller
bool CollObj::operator <(const CollObj &other) const{
    return x < other.x;
}


//su dung cho make unique
bool CollObj::operator ==(const CollObj &other)const{

    if(b[0] == other.b[0])                        return true;
    if(!other.bw && b[0]== other.b[1])            return true;
    if(!bw && b[1] == other.b[0])                 return true;
    if(!bw && !other.bw && b[1] == other.b[1])    return true;
    return false;
}

