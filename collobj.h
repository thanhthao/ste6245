#ifndef COLLOBJ_H
#define COLLOBJ_H

#include "ball.h"
#include "plane.h"

class CollObj {

private:

    Ball        *b[2];
    Plane       *w;
    double      x;   //bao nhieu thoi gian da su dung de co collision
    bool        bw;  //check that is a ball- wall colision or bbcol

public:
    CollObj(); //need empty constructor because using an array of ball
    CollObj(Ball *b1,Ball *b2,double x);// collision of ball and another at the time x
    CollObj(Ball *b1,Plane *w,double x);//  collision of ball and wall at the time x
    bool operator <(const CollObj &other) const; //using in sort() in controller.cpp
    bool operator ==(const CollObj &other)const; //using in makeUnique() in controller.cpp
    Ball* getBall(int i);
    Plane* getPlane();
    double getX();
    bool getBW();

};

///////////////////////////////////////////////
//         The inline functions             //
//////////////////////////////////////////////

inline
Ball* CollObj::getBall(int i){
    return b[i];
}

inline
Plane* CollObj::getPlane(){
    return w;
}

inline
double CollObj::getX(){
    return x;
}

inline
bool CollObj::getBW(){
    return bw;
}



#endif // COLLOBJ_H
