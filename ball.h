#ifndef BALL_H
#define BALL_H

#include <QDebug>
#include <parametrics/gmpsphere>
#include <parametrics/gmpbeziersurf>

class Ball : public GMlib::PSphere<float> {

private:
    float mass;                         //mass of the ball-khoi luong cua ball
    GMlib::PBezierSurf<float> *surf;    //the surface
    GMlib::Point<float,3> p;            //point that need to go
    GMlib::Point<float,3> q;            //point to start
    GMlib::Vector<float,3> vel;         //velocity of the ball
    float u;                            //coordinates
    float v;
    GMlib::UnitVector<float,3> n;       //normal vector
    GMlib::Vector<float,3> ds;          // direction to move
    GMlib::UnitVector<float,3> rax;     // rotation axis
    float x;                            // how much time is used ???
    double energy;                      //energy


public:
    using PSphere::PSphere;
    Ball(GMlib::Vector<float,3> v,float m,float r,GMlib::PBezierSurf<float>* s);
    ~Ball();
    GMlib::Vector<float,3> get_ds();
    void set_ds(const GMlib::Vector<float,3> &s);
    void updateStep(double dt);
    void setVel(GMlib::Vector<float,3> new_vel);
    void setVelKeepEnergy(GMlib::Vector<float,3> new_vel);
    GMlib::Vector<float,3> getVel();
    float getMass();
    float getX();
    float setX(float x1);
    void setUV(GMlib::PBezierSurf<float>*s);
    void moveBall(double time);
    double getEnergy();
    void moveToGround(GMlib::PBezierSurf<float> *ground,const GMlib::Vector<float,3> vec2); // move the ball to the ground first

protected:
    // dt :time from system
    void localSimulate(double dt) override ;

}; // END class Ball


///////////////////////////////////////////////
//         The inline functions             //
//////////////////////////////////////////////

inline
GMlib::Vector<float,3> Ball::get_ds(){
    return ds;
}

inline
GMlib::Vector<float,3> Ball::getVel(){
    return vel;
}

inline
void Ball::setVel(GMlib::Vector<float,3> new_vel){
    this->vel = new_vel;
}

inline
void Ball::setVelKeepEnergy(GMlib::Vector<float,3> new_vel){
    this->vel = new_vel;
//    this->energy = mass*(0.5*(vel*vel)+ 9.81*getPos()(2));
//    double k;
//    k = std::sqrt(2*(energy/mass + 9.81*getPos()(2)));
//    this->vel = k/new_vel.getLength();

}

inline
float Ball::getMass(){
    return mass;
}

inline
float Ball::getX(){
    return x;
}

inline
float Ball::setX(float x1){
    this->x = x1;
}

inline
double Ball::getEnergy(){
        return this->mass*(0.5*(vel*vel) + 9.81*this->getPos()(2));
}

#endif // BALL_H
